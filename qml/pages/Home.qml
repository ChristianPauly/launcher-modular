import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import MySettings 1.0
import AppHandler 1.0
import Terminalaccess 1.0
import "../widgets"
import QtQuick.Controls 2.2
import Ubuntu.Components.Popups 1.3
import GSettings 1.0



Item {
        id: home

Component {
	id: diag
 Dialog {
	id: popup
	title: "Authentification needed"
	TextField {
		id:inp
		placeholderText: "Enter password (by defaut : phablet)"
		echoMode: TextInput.Password
	}
	Button {
		text:"ok"
		onClicked: {Terminalaccess.inputLine(inp.text, false);PopupUtils.close(popup)}
	}
 }
}
Connections {
	target: Terminalaccess
	onNeedSudoPassword: {PopupUtils.open(diag)}
}

    property bool reloading: false
	function getIcon()
                {
                   home.reloading = true
                   AppHandler.reload()
		   AppHandler.permaFilter()
		           AppHandler.permaFilter("NoDisplay", "^(?!true$).*$") //keep the one that have NOT NoDisplay=true
                   AppHandler.permaFilter("Icon",  "/.*$")
		           //AppHandler.appsinfo.push(settingsButton)
		           AppHandler.sort()
                   home.reloading = false
                }
        Flickable {
                id: flickable
                anchors.fill: parent
                contentHeight: listColumn.childrenRect.height+units.gu(2)
                flickableDirection: Flickable.VerticalFlick
                clip: true
                maximumFlickVelocity : units.gu(2)*100
                flickDeceleration: 2500
                    
                Behavior on contentY{
                    NumberAnimation {
                        duration: 1000
                        easing.type: Easing.OutBounce
                    }
                }
            
           PullToRefresh {
                parent: flickable
                refreshing: home.reloading
                onRefresh: home.getIcon();
               
            } 

                Column {
                    id: listColumn
                    anchors.fill: parent
                    anchors {
                        rightMargin: units.gu(2)
                        leftMargin: units.gu(2)
                        topMargin: units.gu(2)
                    }
       
                    Rectangle {
                        id: search
                        height: units.gu(5)
                        color: "transparent"
                        anchors {
                            left: parent.left
                            right: parent.right
                        }
                        Rectangle {
                            id: colorSearch
                            color: launchermodular.settings.backgroundColor
                            radius: units.gu(1)
                            opacity: 0.3
                            anchors.fill: parent
                        }
                        Icon {
                            id: iconSearch
                            anchors {
                                left: search.left
                                rightMargin: units.gu(1)
                                leftMargin: units.gu(1)
                            }
                            anchors.verticalCenter: parent.verticalCenter
                            height: parent.height*0.5
                            width: height
                            name: "find"
                        }
                        TextField {
                            id: searchField
                            anchors {
                                left: iconSearch.right
                                right: iconWebSearch.left
                            }
                            height: search.height
                            color: launchermodular.settings.textColor
                                background: Rectangle {
                                                        height: parent.height
                                                        color: "transparent"
                                                      }

                            placeholderText: i18n.tr("Search your phone and online sources")
                            inputMethodHints: Qt.ImhNoPredictiveText
                            onVisibleChanged: {
                                if (visible) {
                                    forceActiveFocus()
                                }
                            }
                            onTextChanged: {
                        AppHandler.resetTempFilter()
				if(text.length > 0){
                    iconWebSearch.visible = true
					AppHandler.tempFilter("Name", text)
                    widgetL.visible = false
                    widgetR.visible = false
                    listColumnAppsFavorite.visible = false
                    titleFavorite.visible = false
                    titleList.anchors.top = search.bottom
				}else{
                    iconWebSearch.visible = false
					AppHandler.resetTempFilter()
                    widgetL.visible = true
                    widgetR.visible = true
                    listColumnAppsFavorite.visible = true
                    titleFavorite.visible = true
                    titleList.anchors.top = listColumnAppsFavorite.bottom
                    }
                            }
                        }

                        Icon {
                            id: iconWebSearch
                            visible: false
                            anchors {
                                right: search.right
                                rightMargin: units.gu(1)
                                leftMargin: units.gu(1)
                            }
                            anchors.verticalCenter: parent.verticalCenter
                            height: parent.height*0.5
                            width: height
                            source: "home/assets/websearch.svg"
                            MouseArea {
                                anchors.fill: parent
                                    onClicked:{
                                            if(searchField.text.toLowerCase().startsWith("http://") || searchField.text.toLowerCase().startsWith("https://")) {
                                                Qt.openUrlExternally( searchField.text.toLowerCase() );
                                            }else{
                                                Qt.openUrlExternally( "https://duckduckgo.com/?q="+searchField.text.toLowerCase()+"&t=h_&ia=web" );
                                            }

                                     }
                            }
                        }
                    }


                    Column{
                        id: widgetL
                        anchors.top: search.bottom
                        anchors.left: parent.left
                        width: parent.width * 0.5


                            Clock {}
                            Alarm {}
                            Lastcall {}
                            Lastmessage {}

                    }
                    Column{
                        id: widgetR
                        anchors.top: search.bottom
                        anchors.right: parent.right
                        width: parent.width * 0.5

                        Weather {}
                        Event {}
                    }




                    Rectangle{
                        id: titleFavorite
                        anchors.top: if(widgetL.height < widgetR.height){widgetR.bottom}else{widgetL.bottom}
                        anchors.topMargin: if(launchermodular.favoriteAppsModel.count != "0"){ units.gu(4)}else{units.gu(0)}
                        height: if(launchermodular.favoriteAppsModel.count != "0"){ units.gu(4)}else{units.gu(0)}
                        visible: if(launchermodular.favoriteAppsModel.count != "0"){true}else{false}
                        width: parent.width
                        color: "transparent"
                        Icon {
                           id: iconFavoriteApps
                            width: units.gu(2)
                            height: units.gu(2)
                            name: "starred"
                            color: launchermodular.settings.textColor
                        }
                        GSettings {
    id: settings
        schema.id: "com.canonical.Unity.ClickScope"        
        //onChanged: if (key == "coreApps") { ScopeHelper.invalidateScope("clickscope") }
        //Component.onCompleted: titleFavoriteApps.text ="a " +settings.coreApps.join()
    }
                        Label {
                            id: titleFavoriteApps
                            anchors.left: iconFavoriteApps.right
                            anchors.leftMargin: units.gu(1)
                            text: i18n.tr("Favorite Apps")
                            color: launchermodular.settings.textColor
                        }
                    }
                Rectangle {
                    id: listColumnAppsFavorite
                    anchors.top: titleFavorite.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: flickable.width
                    height: if(launchermodular.favoriteAppsModel.count != "0"){ units.gu(13)}else{units.gu(0)}
                    color: "transparent"


    ListView {
        id: appFavoriteView
        model: launchermodular.favoriteAppsModel
        anchors.fill: parent
        clip: true
        //spacing: units.gu(-1)
        orientation: ListView.Horizontal
            delegate: Item{
                width: gview.cellWidth
                height:units.gu(13)

                        Image {
                            id: screenshotAppFavorite
                            anchors.right: parent.right
                            width: units.gu(8)
                            height: units.gu(8)
                            source: icon
                            visible: if (launchermodular.settings.iconStyle == "none") { true;}else{ false;}
                        }

                        OpacityMask {
                            anchors.fill: screenshotAppFavorite
                            source: screenshotAppFavorite
                            maskSource: Rectangle {
                                width: screenshotAppFavorite.width
                                height: screenshotAppFavorite.height
                                radius: units.gu(8)
                                color: if (launchermodular.settings.iconStyle == "rounded") { "";}else{ "transparent";}
                                visible: if (launchermodular.settings.iconStyle == "rounded") { false;}else{ true;}
                            }
                        }

                        UbuntuShape {
                            source: screenshotAppFavorite
                            anchors.right: parent.right
                            aspect: UbuntuShape.Flat
                            width: if (launchermodular.settings.iconStyle == "default") { units.gu(8);}else{ units.gu(0);}
                            height: if (launchermodular.settings.iconStyle == "default") { units.gu(8);}else{ units.gu(0);}
                            radius : "medium"

                            MouseArea {
                                anchors.fill: parent
                                onClicked: listColumnApps.doAction(action)
                            }
                        }


                Text{
                    anchors.top: screenshotAppFavorite.bottom
                    horizontalAlignment: Text.AlignHCenter
                    anchors.topMargin: units.gu(1)
                    anchors.right: parent.right
                    width: screenshotAppFavorite.width
                    //font.pixelSize: small
                    wrapMode: Text.Wrap
                    text: name
                    color: launchermodular.settings.textColor
                 }

        }
    }

                }



                    Rectangle{
                        id: titleList
                        anchors.top: listColumnAppsFavorite.bottom
                        anchors.topMargin: units.gu(4)
                        height: units.gu(4)
                        Icon {
                           id: iconInstalledApps
                            width: units.gu(2)
                            height: units.gu(2)
                            name: "keypad"
                            color: launchermodular.settings.textColor
                        }
                        Label {
                            id: titleListApps
                            anchors.left: iconInstalledApps.right
                            anchors.leftMargin: units.gu(1)
                            text: i18n.tr("Installed Apps")
                            color: launchermodular.settings.textColor
                        }
                    }

                Rectangle {
                    id: listColumnApps
                    anchors.top: titleList.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: parent.width
                    height: gview.contentHeight
                    color: "transparent"


			function doAction(action) {
				console.log("opening "+action)
				if(action.startsWith("application:///")) {
					Qt.openUrlExternally(action);
				}
				if(action.startsWith("internal:///")) {
					pageStack.push(Qt.resolvedUrl(action.replace(/^internal:\/\/\//, "")))
				}
			}


    GridView {
        id: gview
        anchors.fill: parent

        cellHeight: iconbasesize+units.gu(5)
        property real iconbasesize: units.gu(10)
        cellWidth: Math.floor(width/Math.floor(width/iconbasesize))
            
        focus: true
        model: AppHandler.appsinfo.length
        interactive: false
            
        delegate: Rectangle {
                    width: gview.cellWidth
                    height: gview.iconbasesize
                    color: "transparent"
			property var elem: AppHandler.appsinfo[index]

                    Item {
                        width: units.gu(8)
                        height: units.gu(8)
                            anchors.horizontalCenter: parent.horizontalCenter

                        Image {
                            id: imgIcons
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.verticalCenter: parent.verticalCenter
                            width: units.gu(8)
                            height: units.gu(8)
                            source:elem.icon
                            visible: if (launchermodular.settings.iconStyle == "none") { true;}else{ false;}
                        }

                        OpacityMask {
                            anchors.fill: imgIcons
                            source: imgIcons
                            maskSource: Rectangle {
                                width: imgIcons.width
                                height: imgIcons.height
                                radius: units.gu(8)
                                color: if (launchermodular.settings.iconStyle == "rounded") { "";}else{ "transparent";}
                                visible: if (launchermodular.settings.iconStyle == "rounded") { false;}else{ true;}
                            }
                        }

                        UbuntuShape {
                            source: imgIcons
                            aspect: UbuntuShape.Flat
                            width: if (launchermodular.settings.iconStyle == "default") { parent.width;}else{ units.gu(0);}
                            height: if (launchermodular.settings.iconStyle == "default") { parent.height;}else{ units.gu(0);}
                            radius : "medium"
                        }

                        Text{
                            anchors.top: parent.bottom
                            horizontalAlignment: Text.AlignHCenter
                            anchors.topMargin: units.gu(1)
                            width: parent.width
                            //font.pixelSize: small
                            wrapMode: Text.Wrap
                            text: elem.name
                            color: launchermodular.settings.textColor
                         }

                       Component {
                            id: appsDialog
                            Dialog {
                                id: appsDialogue

                                title: elem.name
                                text: elem.getProp("Comment")


                                Rectangle {
                                    height: units.gu(5)
                                    width: parent.width
                                    Button {
                                        anchors.left: parent.left
                                        id: uninstallButton
                                        text: i18n.tr("Uninstall")
                                        height: units.gu(4)
                                        width: (parent.width/2)-units.gu(2)
                                        contentItem: Text {
                                            text: uninstallButton.text
                                            font: uninstallButton.font
                                            color: "#ffffff"
                                            horizontalAlignment: Text.AlignHCenter
                                            verticalAlignment: Text.AlignVCenter
                                            elide: Text.ElideRight
                                        }

                                        background: Rectangle {
                                            radius: units.gu(1.5)
                                            color: "#E95420"
                                        }

                                        onClicked: {
                                            PopupUtils.close(appsDialogue);
                                            Terminalaccess.run("sudo -S click unregister --user=phablet "+elem.getProp("package_name"))
                                            }
                                    }
                                    Button{
                                        anchors.right: parent.right
                                        text: i18n.tr("Cancel")
                                        height: units.gu(4)
                                        width: (parent.width/2)-units.gu(2)
                                        background: Rectangle {
                                            radius: units.gu(1.5)
                                            color: "#F6F6F5"
                                        }
                                        onClicked: {
                                            onClicked: PopupUtils.close(appsDialogue);
                                        }
                                    }


                                }

                                Component.onCompleted: {
                                // add favorite only if
                                var app_already_in_favoriteApps = false;

                                for (var i=0; i < launchermodular.favoriteAppsModel.count; i++) {
                                    var hmodel = launchermodular.favoriteAppsModel.get(i);
                                    if (hmodel.icon === elem.icon) {
                                        app_already_in_favoriteApps = true;
                                    }
                                }

                                if (app_already_in_favoriteApps) {

                                    addFavorite.visible = false

                                } else {

                                    addFavorite.visible = true

                                }
                                }

                                       Button{
                                        id: addFavorite
                                        visible: false
                                        text: i18n.tr("Add to favorite")
                                        //height: units.gu(4)
                                        background: Rectangle {
                                            radius: units.gu(1.5)
                                            color: "#F6F6F5"
                                        }
                                        onClicked: {
                                                launchermodular.favoriteAppsModel.append({"name": elem.name, "icon": elem.icon, "action": elem.action});
                                                PopupUtils.close(appsDialogue);
                                                       }
                                      }


                            }
                        }

                        // application:///$(app_id).desktop
                        MouseArea {
                            anchors.fill: parent
                                onClicked: {
                                    listColumnApps.doAction(elem.action)
                                }
                                onPressAndHold: {
                                    PopupUtils.open(appsDialog);
                                } // pressAndHold
                        }

                    } // Item
            }// delegate Rectangle

        }

/*    AppInfo {
		id:settingsButton
		name:"Settings launcher"
		action: "internal:///../Settings.qml"
		icon:"../../assets/settings.png"
	}
*/
    Component.onCompleted: {
		console.log(AppHandler.appsinfo.length);
		AppHandler.permaFilter()
		AppHandler.permaFilter("NoDisplay", "^(?!true$).*$") //keep the one that have NOT NoDisplay=true
        AppHandler.permaFilter("Icon",  "/.*$")
		//AppHandler.appsinfo.push(settingsButton)
		AppHandler.sort()
		//console.log(AppHandler.appsinfo[0].name);
		//console.log(AppHandler.appsinfo[0].getProp("package_name"));
		//console.log(AppHandler.appsinfo[0].getProp("Icon"));
		//console.log(Qt.locale().name);
	}

                }
                }
            } //Flickable

}
