import QtQuick 2.4
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItemHeader

Page {
    id: pageSettingsHome

    header: PageHeader {
        id: headerSettings
        title: i18n.tr("Settings Page");
       StyleHints {
           foregroundColor: "#FFFFFF";
           backgroundColor: "#111111";
       }
            leadingActionBar.actions: [
                Action {
                    iconName: "back"
                    text: "Back"
                    onTriggered: {
                        pageStack.pop();
                    }
                }
            ]
   }


Rectangle {
    id:mainsettings
        anchors.fill: parent
        color: "#111111"
        anchors.topMargin: units.gu(6)


    Flickable {
        id: flickableSettings
        anchors.fill: parent
        contentHeight: settingsColumn.height
        flickableDirection: Flickable.VerticalFlick
        clip: true


        Column {
            id: settingsColumn
            anchors.fill: parent


            ListItemHeader.Header {
                id: titleFavoriteAppsManagement
                text: "<font color=\"#ffffff\">"+i18n.tr("Favorite apps management")+"</font>"
            }

        ListView {
            model: launchermodular.favoriteAppsModel
            anchors.top: titleFavoriteAppsManagement.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: contentHeight
            delegate: ListItem {
                divider.visible: false
                height: modelLayout.height + (divider.visible ? divider.height : 0)
            ListItemLayout {
                id: modelLayout
                title.text: "<font color=\"#ffffff\">"+name+"</font>"
                    UbuntuShape {
                        source: Image {
                            id: screenshotAppFavorite
                            source: model.icon
                            smooth: true
                            antialiasing: true
                        }
                        SlotsLayout.position: SlotsLayout.Leading;
                        width: units.gu(4)
                        height: width
                        radius : "medium"
                    }
            }

        leadingActions: ListItemActions {
            actions: [
                Action {
                    id: actionDelete
                    text: i18n.tr("Delete")
                    iconName: "edit-delete"
                    onTriggered: launchermodular.favoriteAppsModel.remove(index)
                }
            ]
        }
            onPressAndHold: {
                ListView.view.ViewItems.dragMode = !ListView.view.ViewItems.dragMode
            }

            }
            ViewItems.onDragUpdated: {
                if (event.status == ListItemDrag.Moving) {
                    model.move(event.from, event.to, 1);
                }
            }
            moveDisplaced: Transition {
                UbuntuNumberAnimation {
                    property: "y"
                }
            }
        }



        } // column
    } //flickable
 } //rectangle settings



}
