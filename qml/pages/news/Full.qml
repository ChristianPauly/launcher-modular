import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0
import QtQuick.Controls 2.2
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import "../../../widgets"
import Terminalaccess 1.0
import Ubuntu.Components.Popups 0.1


Page {
    id: pageNews
    property string fullText: ""
    property string title: ""

    header: PageHeader {
        id: headerSettings
        title: pageNews.title;
       StyleHints {
           foregroundColor: "#FFFFFF";
           backgroundColor: "#111111";
       }
            leadingActionBar.actions: [
                Action {
                    iconName: "back"
                    text: "Back"
                    onTriggered: {
                        pageStack.pop();
                    }
                }
            ]        
   }
    
Rectangle {
    id:mainsettings
        anchors.fill: parent
        color: "#111111"
        anchors.topMargin: units.gu(6)
            
    
    Flickable {
        id: flickableSettings
        anchors.fill: parent
        contentHeight: settingsColumn.height
        flickableDirection: Flickable.VerticalFlick
        clip: true

	Text {
		width:parent.width
		wrapMode:Text.WordWrap
		color:"white"
		text: pageNews.fullText
	}
    } //flickable
 } //rectangle settings
    
    
    
}
