import QtQuick 2.4
import QtQuick.Controls 2.2
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import AppHandler 1.0
import Terminalaccess 1.0
    
Page {
    id: pageSettingsNews

    header: PageHeader {
        id: headerSettings
        title: i18n.tr("Settings Page");
       StyleHints {
           foregroundColor: "#FFFFFF";
           backgroundColor: "#111111";
       }
            leadingActionBar.actions: [
                Action {
                    iconName: "back"
                    text: "Back"
                    onTriggered: {
                        pageStack.pop();
                    }
                }
            ]        
   }    
    
    

Rectangle {
    id:mainsettings
        anchors.fill: parent
        color: "#111111"
        anchors.topMargin: units.gu(6)
            
    
    Flickable {
        id: flickableSettings
        anchors.fill: parent
        contentHeight: settingsColumn.height
        flickableDirection: Flickable.VerticalFlick
        clip: true


        Column {
            id: settingsColumn
            anchors.fill: parent


ListModel { id: appNewsModel }

	Component.onCompleted: {
		for(var i = 0 ; i < AppHandler.appsinfo.length ; i++) {
			if(AppHandler.appsinfo[i].getProp("X-Ubuntu-Launcher") != "")
			{
				var item = AppHandler.appsinfo[i];
                appNewsModel.append({"name": item.name})
			}
		}
	}
        
            ListItem.Header {
                id: titlenewsAppsManagement
                text: "<font color=\"#ffffff\">"+i18n.tr("Management of news sources")+"</font>"
            }
    
        ListView {
            model: appNewsModel
            //anchors.fill: parent
            anchors.top: titlenewsAppsManagement.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: contentHeight

            delegate: ListItem.Standard {
                divider.visible: false
                text: "<font color=\"#ffffff\">"+name+"</font>"
                control: Switch {
                    checked: if( launchermodular.settings.selectedAppNews.indexOf(name) > -1 ) {true}
                    onClicked: if( checked == true ) {
                        launchermodular.settings.selectedAppNews.push(name)
                        }else{
                        var indexApp = launchermodular.settings.selectedAppNews.indexOf(name);
                        if (indexApp !== -1) launchermodular.settings.selectedAppNews.splice(indexApp, 1);
                        }
                }
          }  
        }
    
            
            
        } // column
    } //flickable
 } //rectangle settings
     
    
    
}
