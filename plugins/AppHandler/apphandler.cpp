#include <QString>
#include <QStringList>
#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDebug>
#include <QQmlListProperty>
#include <QRegularExpression>

#include "apphandler.h"
#include "appinfo.h"

#define APP_SYS_PATH "/usr/share/applications/"
#define APP_USR_PATH "/home/phablet/.local/share/applications/"

AppHandler::AppHandler() {
	loadAppsInfo();
}

QList<AppInfo*> AppHandler::getApps()
{
	return _appinfos;
}
QQmlListProperty<AppInfo> AppHandler::appsinfo()
{
	return QQmlListProperty<AppInfo>(this, 0, &AppHandler::append_appinfo, &AppHandler::count_appinfo, &AppHandler::at_appinfo, &AppHandler::clear_appinfo);
}
void AppHandler::loadAppsInfo()
{
	loadAppsFromDir(APP_SYS_PATH);
	loadAppsFromDir(APP_USR_PATH);
}
void AppHandler::loadAppsFromDir(const QString& path)
{
	QDir dir(path);
	QStringList nameFilters;
	nameFilters << "*.desktop";
	QStringList fileList = dir.entryList(nameFilters, QDir::Files);
	foreach (const QString &fileName, fileList) {	
		QFile file(dir.filePath(fileName));
		file.open(QIODevice::ReadOnly | QIODevice::Text);
		QTextStream filestream(&file);
		filestream.setCodec("UTF-8");
		_appinfos.append(new AppInfo(fileName.left(fileName.size() - QString(".desktop").size()), filestream.readAll()));
		if(!_appinfos.last()->getProp("X-Ubuntu-UAL-Source-Desktop").isEmpty()) {
			QFile subfile(_appinfos.last()->getProp("X-Ubuntu-UAL-Source-Desktop"));
			subfile.open(QIODevice::ReadOnly | QIODevice::Text);
			QTextStream subfilestream(&subfile);
			subfilestream.setCodec("UTF-8");
			_appinfos.last()->import(subfilestream.readAll());
		}
	}
	qDebug() << _appinfos.size() << " desktop file read from " << path;
}
void AppHandler::append_appinfo(QQmlListProperty<AppInfo> *list, AppInfo *appinfo)
{
	AppHandler *appinfoBoard = qobject_cast<AppHandler*>(list->object);
	if(appinfo) {
		appinfoBoard->_appinfos.append(appinfo);
		emit appinfoBoard->appinfoChanged();
	}
		
}
AppInfo* AppHandler::at_appinfo(QQmlListProperty<AppInfo> *list, int index) {
	AppHandler *apphandler = qobject_cast<AppHandler*>(list->object);
	return apphandler->_appinfos.at(index);
}
int AppHandler::count_appinfo(QQmlListProperty<AppInfo> *list) {
	AppHandler *apphandler = qobject_cast<AppHandler*>(list->object);
	return apphandler->_appinfos.size();
}
void AppHandler::clear_appinfo(QQmlListProperty<AppInfo> *list) {
	AppHandler *apphandler = qobject_cast<AppHandler*>(list->object);
	apphandler->_appinfos.clear();
	emit apphandler->appinfoChanged();
}
void AppHandler::reload() {
	_appinfos.clear();
	loadAppsInfo();
}
void AppHandler::permaFilter(const QString& key, const QString& regexp) {
	for(int i=0;i< _appinfos.size();) {
		if(!_appinfos[i]->getProp(key).contains(QRegularExpression(regexp))) {
			delete _appinfos.takeAt(i);
		}
		else
			i++;
	}
	emit appinfoChanged();
}
void AppHandler::sort(const QString& key, bool revertsort) {
	bool ordered;
	do {
		ordered = true;
		for(int i=0;i< _appinfos.size()-1;i++) {
			if( _appinfos[i]->getProp(key) > _appinfos[i+1]->getProp(key)) {
				ordered = false;
				_appinfos.swap(i,i+1);	
			}	
		}
	}
	while (!ordered);
	emit appinfoChanged();
}


void AppHandler::tempFilter(const QString& keys, const QString& regexp, bool caseinsensitive) {
	QStringList key_list = keys.split(";");
	for(int i=0;i< _appinfos.size();) {
		bool filtered = true;
		foreach( const QString& key, key_list) {
			if(_appinfos[i]->getProp(key).contains(QRegularExpression(regexp, (caseinsensitive) ? QRegularExpression::CaseInsensitiveOption : QRegularExpression::NoPatternOption)))
				filtered = false;
		}
		if(filtered == true) {
			_hideByFilter << _appinfos.takeAt(i);
		}
		else
			i++;
	}
	emit appinfoChanged();
}
void AppHandler::resetTempFilter() {
	while(_hideByFilter.size() >0)		
		_appinfos << _hideByFilter.takeFirst();
	
	emit appinfoChanged();
}

/*QString AppHandler::getAppNews() {
	QString xmlNews;
	for(int i=0;i < _appinfos.size();i++) {
		if(!_appinfos[i]->getProp(XML_NEWS_KEY).isEmpty()) {
			Terminalaccess t;
			if(t.run(_appinfos[i]->getProp("Path")+_appinfos[i]->getProp("Exec").split(" ").last()+" "+_appinfos[i]->getProp(XML_NEWS_KEY)+" 2> /dev/null"))
				xmlNews+=t.outputUntilEnd();
		}
	}
	return xmlNews;
}*/
