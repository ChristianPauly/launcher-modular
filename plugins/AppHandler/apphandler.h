#ifndef APPHANDLER_H
#define APPHANDLER_H

#include <QObject>
#include <QString>
#include <QQmlListProperty>
#include "appinfo.h"

class AppHandler: public QObject {
	Q_OBJECT
	Q_PROPERTY(QQmlListProperty<AppInfo> appsinfo READ appsinfo NOTIFY appinfoChanged)

public:
	AppHandler();
	~AppHandler() = default;
	QList<AppInfo*> getApps();
	QQmlListProperty<AppInfo> appsinfo();

	Q_INVOKABLE void reload();
	Q_INVOKABLE void permaFilter(const QString& key = "Terminal", const QString& regexp = "false");
	Q_INVOKABLE void sort(const QString& key ="Name", bool revertsort = false);
	Q_INVOKABLE void resetTempFilter();
	Q_INVOKABLE void tempFilter(const QString& keys, const QString& regexp, bool caseinsensitive = true);

public slots:
signals:
	void appinfoChanged();
protected:
	QList<AppInfo*> _appinfos;
	QList<AppInfo*> _hideByFilter;
private:
	void loadAppsFromDir(const QString& path);
	void loadAppsInfo();
	static void append_appinfo(QQmlListProperty<AppInfo> *list, AppInfo *appinfo);
	static AppInfo* at_appinfo(QQmlListProperty<AppInfo> *list, int at);
	static int count_appinfo(QQmlListProperty<AppInfo> *list);
	static void clear_appinfo(QQmlListProperty<AppInfo> *list);
};
#endif
