#!/bin/bash

mount -o rw,remount /

mv -f /usr/bin/unity8-dash.bak /usr/bin/unity8-dash

mount -o ro,remount /
