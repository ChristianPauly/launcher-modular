#!/bin/bash

mount -o rw,remount /

mv /usr/bin/unity8-dash /usr/bin/unity8-dash.bak
if [ -f "/usr/bin/unity8-dash.bak" ]; then
    echo '#!/bin/bash
    launched=$(ubuntu-app-list | grep launchermodular.ubuntouchfr)
    appid=$(ubuntu-app-launch-appids | grep launchermodular.ubuntouchfr)
    if [ -z $launched ] && [ -n appid ]
    then
    ubuntu-app-launch $appid
    fi
    ' >> /usr/bin/unity8-dash
    chmod ugo+x /usr/bin/unity8-dash

fi  


mount -o ro,remount /
